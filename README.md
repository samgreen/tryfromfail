# `tryfromfail`

[![Latest Version](https://img.shields.io/crates/v/tryfromfail.svg)](https://crates.io/crates/tryfromfail)
[![Documentation](https://docs.rs/tryfromfail/badge.svg)](https://docs.rs/tryfromfail/)

Custom derive failing `TryFrom` implementations for a set of target types.

Returns the original value as `Err(value)`.

## Example

```rust
#[macro_use]
extern crate tryfromfail;

use std::convert::TryInto;

#[derive(Debug, PartialEq, FailingTryFrom)]
#[FailingTryFrom(OtherType)]
struct SomeType {}

#[derive(Debug, PartialEq)]
struct OtherType {}

#[test]
fn works() {
    let val = OtherType {};
    let failed_try_from: Result<SomeType, OtherType> = val.try_into();
    assert_eq!(failed_try_from, Err(OtherType {}));
}
```
