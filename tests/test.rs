#[macro_use]
extern crate tryfromfail;

use std::convert::TryInto;

#[derive(Debug, PartialEq, FailingTryFrom)]
#[FailingTryFrom(OtherType, AnotherType)]
struct SomeType {}

#[derive(Debug, PartialEq)]
struct OtherType {}

#[derive(Debug, PartialEq)]
struct AnotherType {}

#[test]
fn works_first_target() {
    let val = OtherType {};
    let failed_try_from: Result<SomeType, OtherType> = val.try_into();
    assert_eq!(failed_try_from, Err(OtherType {}));
}

#[test]
fn works_second_target() {
    let val = AnotherType {};
    let failed_try_from: Result<SomeType, AnotherType> = val.try_into();
    assert_eq!(failed_try_from, Err(AnotherType {}));
}
